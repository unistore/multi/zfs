# ZFS

# Features:
list:
- https://www.phoronix.com/scan.php?page=news_item&px=ZFS-On-Linux-0.8-Released

Device Removal
- https://youtu.be/KGnFhmG8gT0

Pool Checkpoint
- https://sdimitro.github.io/post/zpool-checkpoint/

# Guide
Top:
- https://pve.proxmox.com/wiki/ZFS_on_Linux
- https://passthroughpo.st/zfs-configuration-linux-setup-basics/
- https://passthroughpo.st/zfs-configuration-part-2-zvols-lz4-arc-zils-explained/



# doc
- https://wiki.archlinux.org/index.php/ZFS
